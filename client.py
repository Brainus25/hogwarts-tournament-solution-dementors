import logging
import time

logging.basicConfig(filename='client.log', level=logging.DEBUG)

mode = 0
launch = 0


def step(snake_coordinates, food_coordinates, direction, field_size):
    """
    :param snake_coordinates: An array that describes current coordinates of snake on the field
        Eg. [[4,10], [4,9], [4,8]]
    :param food_coordinates: An array that describes current coordinates of food on the field
        Eg. [10,20]
    :param direction: A string that describes current direction of movement of snake. The following values
        are possible: 'up', 'down', 'left', 'right'
    :param field_size: A dict. Eg. {'height': 20, 'width': 60}
    :returns: A string that describes new snake's direction. The following values are acceptable:
        'up', 'down', 'left', 'right'

    """
    global mode
    global launch

    if launch == 0:
        launch = time.time()
    else:
        if time.time() - launch > 59.8:
            if direction == 'left':
                return 'right'
            if direction == 'right':
                return 'left'
            if direction == 'down':
                return 'up'
            return 'down'

    l = len(snake_coordinates)
    x = snake_coordinates[0][1]
    y = snake_coordinates[0][0]
    tailX = snake_coordinates[l - 2][1]
    tailY = snake_coordinates[l - 2][0]
    foodX = food_coordinates[1]
    foodY = food_coordinates[0]
    w = field_size.get('width')
    h = field_size.get('height')

    if x == w - 2 and y != h - 2:
        mode = 1

    if x > 1 and y == h - 2:
        mode = 2
        if l/22+2 < foodX < x and foodY < y and direction == 'left':
            chx = x
            chy = y - 1
            logging.debug("check up")
            for snake in snake_coordinates:
                sX = snake[1]
                sY = snake[0]
                if cross(sX, sY, foodX, foodY, chx, chy):
                    return 'left'
            mode = 1
            logging.debug("check up ok")
            return 'up'
        return 'left'

    if mode == 0 and foodX < x:
        mode = 1
        if direction == 'up':
            return 'right'

    if mode == 0:
        if direction == 'right' or direction == 'up':
            if foodY < y:
                return 'up'
        if direction == 'right' or direction == 'down':
            if foodY > y:
                if foodY == h - 2 and foodX != x:
                    return 'right'
                return 'down'
        if foodX > x:
            return 'right'
        mode = 1

    if mode == 1 or mode == 3:
        if y == foodY and x > foodX > l/22+2 and direction != 'right':
            for snake in snake_coordinates:
                sX = snake[1]
                sY = snake[0]
                if sY == y and foodX < sX < x:
                    if y == h - 3:
                        mode = 2
                    return 'down'
            if x == 2:
                mode = 2
            return 'left'
        if y == foodY and foodX > x and direction != 'left':
            for snake in snake_coordinates:
                sX = snake[1]
                sY = snake[0]
                if sY == y and foodX > sX > x:
                    if y == h - 3:
                        mode = 2
                    return 'down'
            return 'right'
        if direction == 'up':
            return 'up'
        if l/22+2 < foodX < x and foodY < y:
            chx = x
            chy = y
            if direction == 'down':
                chx -= 1
            if direction == 'left':
                chy -= 1
            for snake in snake_coordinates:
                sX = snake[1]
                sY = snake[0]
                if cross(sX, sY, foodX, foodY, chx, chy):
                    if y == h - 3:
                        mode = 2
                    return 'down'
            if direction == 'down':
                return 'left'
            return 'up'
        if y == h - 3:
            mode = 2
        return 'down'

    if mode == 2:
        if x == 1 and tailX == 1 and y < tailY and foodX > 1:
            mode = 0
            return 'right'
        if x == 1 and y == 2:
            mode = 4

        if x > 1:
            return 'left'
        return 'up'

    if mode == 4:
        if x > tailX:
            mode = 0
        if x == 1 and y == 1:
            return 'right'
        if x % 2 == 0:
            if y == h - 3:
                return 'right'
            return 'down'
        if y == 1:
            return 'right'
        return 'up'


def cross(snakeX, snakeY, x1, y1, x2, y2):
    return x1 <= snakeX <= x2 and y1 <= snakeY <= y2
